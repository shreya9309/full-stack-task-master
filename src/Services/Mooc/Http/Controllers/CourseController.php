<?php
namespace App\Services\Mooc\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Mooc\Features\Course\Lists;
use App\Services\Mooc\Features\Course\ProviderList;

class CourseController extends Controller
{
    public function index()
    {
        return $this->serve(Lists::class);
    }

    public function provider()
    {
        return $this->serve(ProviderList::class);
    }
}