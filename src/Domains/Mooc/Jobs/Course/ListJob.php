<?php

namespace App\Domains\Mooc\Jobs\Course;

use Lucid\Foundation\Job;

use App\Data\Models\Courses;
use Carbon\Carbon;

class ListJob extends Job
{
  private $query;
  private $limit;

  public function __construct($query, $limit = 25)
  {
    $this->query = $query;
    $this->limit = $limit;
  }

  public function handle()
  {
    $query = (new Courses)->newQuery();
    $query->selectRaw('*, DATE_FORMAT(course_tbl.next_session_date, "%d %M %Y") as new_next_session_date');
    if (empty($this->query['sort_length']) && empty($this->query['sort_date'])) {
      $query->orderBy('id','ASC');
    }
    if(!empty($this->query['search_param'])){
      $query->where(function($query){
        $query->where('child_subject', 'like', '%'.$this->query['search_param'].'%');
        $query->OrWhere('provider', 'like', '%'.$this->query['search_param'].'%');
      });
    }
    if(!empty($this->query['provider_select'])){
      $query->where('provider',$this->query['provider_select']);
    }
    if(!empty($this->query['from_date'])){
      $query->whereDate('next_session_date','>=', Carbon::parse($this->query['from_date'])->format('Y-m-d'));
    }
    if(!empty($this->query['to_date'])){
      $query->whereDate('next_session_date','<=', Carbon::parse($this->query['to_date'])->format('Y-m-d'));
    }
    if(!empty($this->query['sort_length'])){
      if ($this->query['sort_length']=="asc") {
        $query->orderBy('length','ASC');
      }
      else {
        $query->orderBy('length','DESC');
      }
    }
    if(!empty($this->query['sort_date'])){
      if ($this->query['sort_date']=="asc") {
        $query->orderBy('next_session_date','ASC');
      }
      else {
        $query->orderBy('next_session_date','DESC');
      }
    }
    if(isset($this->query['pagination']) && ($this->query['pagination'] == 'required')) {
      if(isset($this->query['limit']) && !empty($this->query['limit'])) {
        $this->limit = $this->query['limit'];
      }
      $result = $query->paginate($this->limit);
    } else {
      $result = $query->get();
    }
    return $result;
  }
}