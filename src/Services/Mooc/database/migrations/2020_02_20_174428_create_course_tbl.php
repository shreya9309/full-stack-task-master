<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourseTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_tbl', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('course_id')->unique();
            $table->string('course_name',50);
            $table->string('provider')->nullable();
            $table->string('university')->nullable();
            $table->string('parent_subject')->nullable();
            $table->string('child_subject')->nullable();
            $table->string('url')->nullable();
            $table->datetime('next_session_date')->nullable();
            $table->integer('length')->nullable();
            $table->string('video')->nullable();
            $table->timestamps();
            $table->datetime('modified_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_tbl');
    }
}
