<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>{{ config('app.name') }} :: Login </title>
      <!-- Global stylesheets -->
      <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
      <link href="{{ asset('theme/admin/global_assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
      <link href="{{ asset('theme/admin/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
      <link href="{{ asset('theme/admin/assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
      <link href="{{ asset('theme/admin/assets/css/layout.min.css') }}" rel="stylesheet" type="text/css">
      <link href="{{ asset('theme/admin/assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
      <link href="{{ asset('theme/admin/assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
      <!-- /global stylesheets -->
      <!-- Core JS files -->
      <script src="{{ asset('theme/admin/global_assets/js/main/jquery.min.js') }}"></script>
      <script src="{{ asset('theme/admin/global_assets/js/main/bootstrap.bundle.min.js') }}"></script>
      <script src="{{ asset('theme/admin/global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
      <script src="{{ asset('theme/admin/global_assets/js/plugins/ui/ripple.min.js') }}"></script>
      <!-- /core JS files -->
      <!-- Theme JS files -->
      <script src="{{ asset('theme/admin/assets/js/app.js') }}"></script>
      <!-- /theme JS files -->
      <style>

      .body-bg {
      	background-color: #fff;
      }

      .login-btn {
      	color: #fff;
        background-color: #02A7F0;
      	padding: 8px 25px;
      }

      .login-btn:hover {
        background-color: #f44336;
      }


      .login-card {
          position: relative;
          display: -ms-flexbox;
          display: flex;
          -ms-flex-direction: column;
          flex-direction: column;
          min-width: 0;
          word-wrap: break-word;

      }

      .login-card-body {
          -ms-flex: 1 1 auto;
          flex: 1 1 auto;
      }

      @media only screen and (max-width: 1100px) {
      	.login-card-body {
      	   padding: 1.25rem;
      	}
      	.login-card {
          background-color: #fff;
          background-clip: border-box;
      		border: 1px solid rgba(0,0,0,.125);
      		border-radius: .1875rem;
      		box-shadow: 0 1px 2px rgba(0,0,0,.05);
      	}

      	.body-bg {
      	  /* The image used */
      		background-image:url("{{ asset('theme/admin/images/login_bg.png') }}");
      		background-repeat:no-repeat;
      		background-position:cover;
      		background-color: #fff;
      	}

      }

      </style>
</head>

<body class="body-bg">

	<!-- Page content -->
	<div class="page-content">

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Content area -->
			<div class="content d-flex justify-content-center align-items-center">

				<!-- Login form -->
        <form action="{{ route('admin.login') }}" method="POST" id="admin_login_form" class="login-form">
           {{ csrf_field() }}
					<div class="login-card mb-0">
						<div class="login-card-body">
							<div class="mb-3">
								<h2 class="mb-0">Please login!!!!</h2>
							</div>
              @if (Session::has('error' ))
                <p class="alert alert-warning border-0 alert-dismissible" style="box-shadow:none;">{{ Session::get('error') }}</p>
              @endif
							<div class="form-group">
								<label class="col-form-label">Login Email Id</label>
								<input type="text" name="email" class="form-control form-control-lg {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Enter Email" autofocus value="{{ old('email') }}">
                @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                {{ $errors->first('email') }}
                </span>
                @endif
							</div>
							<div class="form-group">
								<label class="col-form-label">Login Password</label>
								<input type="password" name="password" class="form-control form-control-lg  {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Enter Password" value="{{ old('password') }}">
                @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                {{ $errors->first('password') }}
                </span>
                @endif
							</div>

							<div class="text-right mb-2">
							</div>
							<div class="form-group text-center">
								<button type="submit" class="btn login-btn rounded-round">Login <i class="icon-circle-right2 ml-2"></i></button>
							</div>

						</div>
					</div>
				</form>
				<!-- /login form -->

			</div>
			<!-- /content area -->
		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->
  <!-- /page content -->
  <script src="{{ asset('theme/global/validator/bootstrapValidator.min.js')}}"></script>
  <script>
     $( document ).ready(function() {

       $('#admin_login_form').bootstrapValidator({
             message: 'This value is not valid',
             excluded: [':disabled'],
                fields: {
                 email: {
                       validators: {
                         notEmpty: {
                             message: 'Email is required'
                         },
                         regexp: {
                             regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                             message: 'Enter valid email address'
                         },
                     }
                 },
                 password: {
                   validators: {
                       notEmpty: {
                           message: 'Password  is required'
                       },
                       stringLength: {
                          min: 6,
                          max: 13,
                          message: 'The password must between 6 - 13 characters'
                      },
                     }
                  }
               }
         }).on('success.field.bv', function(e, data) {
                 var $parent = data.element.parents('.form-group');
                 $parent.removeClass('has-success');
         }).on('success.form.bv', function(e, data) {

         });
     });
  </script>
</body>
</html>
