<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>{{ config('app.name', 'Laravel') }}</title>
      <!-- Global stylesheets -->
      <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
      <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
      <link href="{{ asset('theme/admin/global_assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
      <link href="{{ asset('theme/admin/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
      <link href="{{ asset('theme/admin/assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
      <link href="{{ asset('theme/admin/assets/css/layout.min.css') }}" rel="stylesheet" type="text/css">
      <link href="{{ asset('theme/admin/assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
      <link href="{{ asset('theme/admin/assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
      <link href="{{ asset('theme/admin/assets/css/style.css') }}" rel="stylesheet" type="text/css">
      <!-- /global stylesheets -->
      <!-- Core JS files -->
      <script src="{{ asset('theme/admin/global_assets/js/main/jquery.min.js') }}"></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
      <script src="{{ asset('theme/admin/global_assets/js/main/bootstrap.bundle.min.js') }}"></script>
      <script src="{{ asset('theme/admin/global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
      <script src="{{ asset('theme/admin/global_assets/js/plugins/ui/slinky.min.js') }}"></script>
      <!-- /core JS files -->
      <script src="{{ asset('theme/admin/global_assets/js/plugins/ui/sticky.min.js') }}"></script>
      <script src="{{ asset('theme/admin/assets/js/app.js') }}"></script>
      <script src="{{ asset('theme/admin/global_assets/js/plugins/navbar/navbar_multiple_sticky.js') }}"></script>
      <script src="{{ asset('theme/admin/assets/js/custom.js') }}"></script>
      <script src="{{ asset('theme/admin/global_assets/js/plugins/ui/moment/moment.min.js') }}"></script>
      <script src="{{ asset('theme/admin/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
      <script src="{{ asset('theme/admin/global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
      <script src="{{ asset('theme/admin/global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
      <script src="{{ asset('theme/admin/global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
      <script src="{{ asset('theme/admin/global_assets/js/plugins/pickers/daterangepicker.js') }}"></script>
      <script src="{{ asset('theme/admin/global_assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
      <script src="{{ asset('theme/admin/global_assets/js/plugins/notifications/noty.min.js') }}"></script>
      <script src="{{ asset('theme/admin/global_assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>
   </head>
   <body>
      @include('mooc::layouts.header')
      @yield('content')
      @include('mooc::layouts.footer')
   </body>
</html>
