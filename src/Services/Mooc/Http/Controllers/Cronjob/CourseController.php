<?php

namespace App\Services\Mooc\Http\Controllers\Cronjob;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;

use App\Services\Mooc\Features\Cronjob\Course\UpdateFeature;

class CourseController extends Controller
{
    public function __construct()
    {
    }

    public function update()
    {
      return $this->serve(UpdateFeature::class);
    }
}