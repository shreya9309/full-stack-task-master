<?php

namespace App\Domains\Http\Jobs\Respond\Listing;

use Lucid\Foundation\Job;
use Illuminate\Routing\ResponseFactory;

class RespondWithJsonJob extends Job
{
    protected $status;
    protected $content;
    protected $headers;
    protected $options;
    protected $response;
    protected $pagination;
    protected $filters;

    public function __construct($content, $status = 200, array $headers = [], $options = 0, $response = [],$pagination,$filters)
    {
        $this->content = $content;
        $this->status = $status;
        $this->headers = $headers;
        $this->options = $options;
        $this->response = $response;
        $this->pagination = $pagination;
        $this->filters = $filters;
    }

    public function handle(ResponseFactory $factory)
    {
        $response = [
            'status' => 'success',
            'response' => [
              'code' => 200,
              'type' => $this->response['type'],
              'message' => $this->response['message'],
            ],
            'data' => $this->content,
            'pagination' => $this->pagination,
            'filters' => $this->filters,
        ];

        return $factory->json($response, $this->status, $this->headers, $this->options);
    }
}
