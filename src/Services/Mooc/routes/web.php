<?php

/*
|--------------------------------------------------------------------------
| Service - Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for this service.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'mooc'], function() {

    // The controllers live in src/Services/Mooc/Http/Controllers
    // Route::get('/', 'UserController@index');

    Route::get('/', function() {
        return view('mooc::welcome');
    });

});
Route::group(['prefix' => 'admin'], function() {
  Route::get('login', 'Authentication@index')->name('admin.login');
  Route::post('login', 'Authentication@login')->name('admin.login');
  Route::get('logout', 'Authentication@logout')->name('admin.logout');
  Route::get('dashboard', 'Authentication@dashboard')->name('admin.dashboard');
});