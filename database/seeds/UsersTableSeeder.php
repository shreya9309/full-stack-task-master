<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Data\Models\Users;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      	if (Users::where('email', '=', 'testing@testing.com')->first() === null) {
            Users::create([
                'name'          => 'admin',
                'email'         => 'testing@testing.com',
                'password'      =>  Hash::make('123456'),
            ]);
        }
    }
}
