<?php

namespace App\Domains\Mooc\Jobs\Course;

use Lucid\Foundation\Job;

use App\Data\Models\Courses;
use Carbon\Carbon;

class ProviderListJob extends Job
{
  private $query;
  private $limit;

  public function __construct($query, $limit = 25)
  {
    $this->query = $query;
    $this->limit = $limit;
  }

  public function handle()
  {
    $query = (new Courses)->newQuery();
    if(!empty($this->query['search_param'])){
      $query->where('provider', 'like', '%'.$this->query['search_param'].'%');
    }
    $query->groupby('provider')->orderBy('provider','ASC');
    if(isset($this->query['pagination']) && ($this->query['pagination'] == 'required')) {
      if(isset($this->query['limit']) && !empty($this->query['limit'])) {
        $this->limit = $this->query['limit'];
      }
      $result = $query->paginate($this->limit);
    } else {
      $result = $query->get();
    }
    return $result;
  }
}