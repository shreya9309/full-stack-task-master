@extends('mooc::layouts.main')
@section('content')
<div class="page-content mt-3 ml-3 mr-3 p-1">
  <div class="content-wrapper">
    <div class="card">
      <div class="card-body pt-3">
        <div class="row">
          <div class="col-md-12">
            <form class="row pt-1 pb-1 mt-1" id="filter_course_table">
              <div class="col-xl-2">
                <button type="button" class="btn btn-outline bg-pink-400 border-pink-400 text-pink-800 btn-icon legitRipple" id="store_course">Store/Dump course details</button>
              </div>
              <div class="col-xl-2">
                <select name="provider_select" class="form-control provider_select"></select>
              </div>
              <div class="col-xl-2">
                <!-- <label>From Date</label> -->
                <div class="input-group">
                  <input type="text" class="form-control" readonly name="from_date" id="from_date" placeholder="Select next session start date">
                </div>
              </div>
              <div class="col-xl-2">
                <!-- <label>To Date</label> -->
                <div class="input-group">
                  <input type="text" class="form-control" readonly name="to_date" id="to_date" placeholder="Select next session end date">
                </div>
              </div>
              <div class="col-xl-2">
                <button type="button" class="mb-1 btn btn-outline bg-slate-400 border-slate-400 text-slate-800 btn-icon legitRipple" id="sort_length_asc">Sort by Length ASC</button>
                <button type="button" class="btn btn-outline bg-slate-400 border-slate-400 text-slate-800 btn-icon legitRipple" id="sort_length_desc">Sort by Length DESC</button>
                <input type="hidden" name="sort_length" value="">
              </div>
              <div class="col-xl-2">
                <button type="button" class="mb-1 btn btn-outline bg-slate-400 border-slate-400 text-slate-800 btn-icon legitRipple" id="sort_date_asc">Sort by Next session date ASC</button>
                <button type="button" class="btn btn-outline bg-slate-400 border-slate-400 text-slate-800 btn-icon legitRipple" id="sort_date_desc">Sort by Next session date DESC</button>
                <input type="hidden" name="sort_date" value="">
              </div>
            </form>
            <table class="table datalist table-bordered">
              <thead>
                <tr>
                  <th></th>
                  <th>Course Id</th>
                  <th>Course Name</th>
                  <th>Provider</th>
                  <th>Universities/Institutions</th>
                  <th>Parent Subject</th>
                  <th>Child Subject</th>
                  <th>Url</th>
                  <th>Next Session Date</th>
                  <th>Length</th>
                  <th>Video(Url)</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  var base_url = {!! json_encode(url('/')) !!};
  $('body').tooltip({
    selector: '[data-popup="tooltip"]',
    trigger : 'hover'
  });
  var DatatableResponsive = function() {
    var _componentDatatableResponsive = function() {
      if (!$().DataTable) {
        console.warn('Warning - datatables.min.js is not loaded.');
        return;
      }
      $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        responsive: true,
        columnDefs: [{
          orderable: false,
          width: 100,
          targets: [ 3 ],
        }],
        dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
          search: '<span>Filter:</span> _INPUT_',
          searchPlaceholder: 'Type to filter...',
          lengthMenu: '<span>Show:</span> _MENU_',
          paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
        }
      });
      $('.datalist').DataTable({
        order: [1, 'asc'],
        ajax: function ( data, callback, settings ) {
          $.ajax({
            url:  base_url + '/api/mooc/list?pagination=required&page=' + (data.start / data.length + 1) +
            '&limit=' + data.length +
            '&search_param=' + $('.dataTables_filter input').val() +
            '&soft_deletes=' + $('[name="soft_deletes"]').val(),
            type: 'GET',
            "data": $('#filter_course_table').serializeArray(),
            contentType: 'application/x-www-form-urlencoded',
            success: function( data, textStatus, jQxhr ){
              if(data.status == 'success'){
                callback({
                  draw: data.draw,
                  data: data.data,
                  recordsTotal:  data.pagination.total,
                  recordsFiltered:  data.pagination.total
                });
              } else {
                callback({
                  draw: data.draw,
                  data: [],
                  recordsTotal:  0,
                  recordsFiltered:  0
                });
              }
            },
            error: function( jqXhr, textStatus, errorThrown ){
            }
          });
        },
        serverSide: true,
        "bAutoWidth": false,
        columns: [
        {
          data: null, "render": function ( data, type, row ) {
          return '';
          }
        },
        {
          data: null, "render": function ( data, type, row ) {
            if (data.course_id) {
              return data.course_id;
            }
            else {
              return '';
            }
          }
        },
        {
          data: null, "render": function ( data, type, row ) {
            if (data.course_name) {
              return data.course_name;
            }
            else {
              return '';
            }
          }
        },
        {
          data: null, "render": function ( data, type, row ) {
            if (data.provider) {
              return data.provider;
            }
            else {
              return '';
            }
          }
        },
        {
          data: null, "render": function ( data, type, row ) {
            if (data.university) {
              return data.university;
            }
            else {
              return '';
            }
          }
        },
        {
          data: null, "render": function ( data, type, row ) {
            if (data.parent_subject) {
              return data.parent_subject;
            }
            else {
              return '';
            }
          }
        },
        {
          data: null, "render": function ( data, type, row ) {
            if (data.child_subject) {
              return data.child_subject;
            }
            else {
              return '';
            }
          }
        },
        {
          data: null, "render": function ( data, type, row ) {
            if (data.url) {
              return '<a href="'+data.url+'" target="_blank">'+data.url+'</a>';
            }
            else {
              return '';
            }
          }
        },
        {
          data: null, "render": function ( data, type, row ) {
            if (data.new_next_session_date) {
              if (data.new_next_session_date=='01 January 1970') {
                return 'Self paced';
              }
              else if (data.new_next_session_date=='01 January 1960') {
                return 'NA';
              }
              else {
                return data.new_next_session_date;
              }
            }
            else {
              return '';
            }
          }
        },
        {
          data: null, "render": function ( data, type, row ) {
            if (data.length) {
              return data.length;
            }
            else {
              return '';
            }
          }
        },
        {
          data: null, "render": function ( data, type, row ) {
            if (data.video) {
              return '<a href="'+data.video+'" target="_blank">'+data.video+'</a>';
            }
            else {
              return '';
            }
          }
        },
        ],
        responsive: {
          details: {
            type: 'column'
          }
        },
        columnDefs: [
        {
          className: 'control',
          orderable: false,
          targets:   0,
        },
        ]
      });
    };
    var _componentSelect2 = function() {
      if (!$().select2) {
        console.warn('Warning - select2.min.js is not loaded.');
        return;
      }
      $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        dropdownAutoWidth: true,
        width: 'auto'
      });
    };
    return {
      init: function() {
        _componentDatatableResponsive();
        _componentSelect2();
      }
    }
  }();

  document.addEventListener('DOMContentLoaded', function() {
    DatatableResponsive.init();
    $("#from_date").datepicker({
      changeMonth: true,
      changeYear: '10',
      autoclose: 'true',
      dateFormat :'dd MM yy',
    }).on('change', function(selectedDate) {
      $("#to_date").datepicker("option", "minDate", $("#from_date").val());
      $('.datalist').DataTable().ajax.reload( null, false );
    });

    $("#to_date").datepicker({
      changeMonth: true,
      changeYear: '10',
      autoclose: 'true',
      dateFormat :'dd MM yy',
    }).on('change', function() {
      $("#from_date").datepicker("option", "maxDate", $("#to_date").val());
      $('.datalist').DataTable().ajax.reload( null, false );
    });

    $(document).on('click','#sort_length_asc', function(e) {
      $('[name="sort_date"]').val('');
      $('[name="sort_length"]').val('asc');
      $('.datalist').DataTable().ajax.reload( null, false );
    });

    $(document).on('click','#sort_length_desc', function(e) {
      $('[name="sort_date"]').val('');
      $('[name="sort_length"]').val('desc');
      $('.datalist').DataTable().ajax.reload( null, false );
    });

    $(document).on('click','#sort_date_asc', function(e) {
      $('[name="sort_length"]').val('');
      $('[name="sort_date"]').val('asc');
      $('.datalist').DataTable().ajax.reload( null, false );
    });

    $(document).on('click','#sort_date_desc', function(e) {
      $('[name="sort_length"]').val('');
      $('[name="sort_date"]').val('desc');
      $('.datalist').DataTable().ajax.reload( null, false );
    });

    $(document).on('click','#store_course', function(e) {
      $("#store_course").html("Please wait for some time...!!");
      $.post(base_url + '/api/mooc/cronjob/update_course', function(result) {
        if(result.status == 'success' && result.response.type == 'success'){
          alert("Data is stored successfully in Database");
          $("#store_course").html("Store/Dump course details");
          $('.datalist').DataTable().ajax.reload( null, false );
        } else {
          alert("Something is wrong!!! check if the correct file exists..");
        }
      }, 'json');
    });

    $('.provider_select').select2({
      placeholder:'Select a provider',
      width: '100%',
      language: {
        noResults: function (params) {
          return "Provider is not found";
        }
      },
      allowClear: true,
      maximumSelectionSize: 1,
      ajax: {
        url: base_url + "/api/mooc/provider",
        dataType: 'json',
        async: true,
        data:{
          pagination:false
        },
        data: function (keyword) {
          return {
            search_param : keyword.term
          };
        },
        processResults: function (data) {
          return {
            results: $.map(data.data, function(value) {
              return {
                text : value.provider,
                id : value.provider
              }
            })
          };
        },
        cache: true
      },
      escapeMarkup: function (markup) { return markup; },
    }).on('change', function() {
      $('.datalist').DataTable().ajax.reload( null, false );
    });
  });
</script>
@stop