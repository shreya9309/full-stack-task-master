<?php

/*
|--------------------------------------------------------------------------
| Service - API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for this service.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Prefix: /api/mooc
Route::group(['prefix' => 'mooc'], function() {

    // The controllers live in src/Services/Mooc/Http/Controllers
    // Route::get('/', 'UserController@index');

    Route::get('/', function() {
        return response()->json(['path' => '/api/mooc']);
    });
    
    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });

    Route::group(['prefix' => 'cronjob'], function() {
        Route::post('update_course', 'Cronjob\CourseController@update');
    });
    Route::get('list', 'CourseController@index');
    Route::get('provider', 'CourseController@provider');
});