<?php

namespace App\Domains\Mooc\Jobs\Cronjob\Course;

use Lucid\Foundation\Job;

use App\Data\Models\Courses;
use Carbon\Carbon;

class UpdateJob extends Job
{
  private $query;

  public function __construct($query)
  {
    $this->query = $query;
  }

  public function handle()
  {
    $status = 'failure';
    if (($handle = fopen ( public_path () . '/course/courses.csv', 'r' )) !== FALSE) {
      while ( ($data = fgetcsv ( $handle, 1000, ',' )) !== FALSE ) {
        $course_data = Courses::where(['course_id' => $data [0]])->first();
        if (empty($course_data)) {
          $course = [];
        }
        else {
          $course = Courses::find($course_data->id);
        }
          $course['modified_at'] = Carbon::now();
          if (!empty($data [0]) && $data [0]!="Course Id") {
            $course['course_id'] = $data [0];
          }
          if (!empty($data [1]) && $data [1]!="Course Name") {
            $course['course_name'] = $data [1];
          }
          if (!empty($data [2]) && $data [2]!="Provider") {
            $course['provider'] = $data [2];
          }
          if (!empty($data [3]) && $data [3]!="Universities/Institutions") {
            $course['university'] = $data [3];
          }
          if (!empty($data [4]) && $data [4]!="Parent Subject") {
            $course['parent_subject'] = $data [4];
          }
          if (!empty($data [5]) && $data [5]!="Child Subject") {
            $course['child_subject'] = $data [5];
          }
          if (!empty($data [6]) && $data [6]!="Url") {
            $course['url'] = $data [6];
          }
          if ($data [7]=="Self paced") {
            $course['next_session_date'] = Carbon::parse('1970-01-01')->format('Y-m-d');
          }
          else if ($data [7]=="NA") {
            $course['next_session_date'] = Carbon::parse('1960-01-01')->format('Y-m-d');
          }
          else if (!empty($data [7]) && $data [7]!="Next Session Date" && $data [7]!="Self paced" && $data [7]!="NA") {
            $data [7] = explode(",", $data [7]);
            $data [7] = array_filter($data [7]);
            $data [7] = implode(" ",$data [7]);
            $date_var = $data [7]." ".substr($data [7], -4);
            $course['next_session_date'] = Carbon::parse($date_var)->format('Y-m-d');
          }
          if (!empty($data [8]) && $data [8]!="Length") {
            $course['length'] = $data [8];
          }
          if (!empty($data [9]) && $data [9]!="Video(Url)") {
            $course['video'] = $data [9];
          }
          if (empty($course_data) && ($data [0]!="Course Id")) {
            $course = Courses::create($course);
          }
          else if ($data [0]!="Course Id") {
            $course->save();
          }
          $status = 'success';
      }
      fclose ( $handle );
    }
    return $status;
  }
}