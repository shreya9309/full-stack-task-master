<?php
namespace App\Services\Mooc\Http\Controllers;

use Illuminate\Http\Request;
use Lucid\Foundation\Http\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
use Route;


class Authentication extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin', ['except' => ['logout','index','login']]);
    }

    public function index()
    {
        return view('mooc::authentication.login');
    }

    public function dashboard()
    {
        $data = [
        ];
        return view('mooc::authentication.dashboard',$data);
    }

    public function login(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'email' => 'required|email',
        'password' => 'required|min:5',
      ]);
      if ($validator->fails())
      {
          return redirect()->back()->withErrors($validator->errors());
      } else {
        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
          return redirect()->intended(route('admin.dashboard'));
        } else {
          return redirect()->route('admin.login')->with('error','Login Failed.')->withInput();
        }
      }
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('admin/login');
    }

}
