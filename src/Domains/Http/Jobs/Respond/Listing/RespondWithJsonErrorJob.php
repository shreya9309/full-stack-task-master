<?php

namespace App\Domains\Http\Jobs\Respond\Listing;

use Illuminate\Routing\ResponseFactory;

class RespondWithJsonErrorJob extends RespondWithJsonJob
{
    protected $status;
    protected $code;
    protected $headers;
    protected $options;
    protected $response;

    public function __construct($status = 200,$code = 200,$headers = [], $options = 0, $response = [])
    {
        $this->response = $response;
        $this->content = [
            'status' => 'failure',
            'response' => [
                'code' => $code,
                'type' => $this->response['type'],
                'message' => $this->response['message'],
            ],
        ];
        $this->status = $status;
        $this->headers = $headers;
        $this->options = $options;
    }

    public function handle(ResponseFactory $factory)
    {
        return $factory->json($this->content, $this->status, $this->headers, $this->options);
    }
}
