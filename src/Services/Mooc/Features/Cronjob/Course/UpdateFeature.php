<?php

namespace App\Services\Mooc\Features\Cronjob\Course;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\Respond\Store\RespondWithJsonJob;

use App\Domains\Mooc\Jobs\Cronjob\Course\UpdateJob;

class UpdateFeature extends Feature
{
    public function handle(Request $request)
    {
	    $response = $this->run(new UpdateJob($request->input()));
	    return $this->run(RespondWithJsonJob::class,[
	        'type' => 'success',
	        'message' => 'Updated successfully',
	    ]);
    }
}