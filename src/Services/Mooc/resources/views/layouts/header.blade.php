<div class="navbar navbar-expand-md navbar-light navbar-sticky" style="background-color:#000000;border-color:#000000">
  <div class="navbar-collapse collapse" id="navbar-navigation">
    <ul class="navbar-nav navbar-nav-highlight">
      <li class="nav-item dropdown">
        <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
          <span>{{ Auth::user()->name }}</span>
        </a>
        <div class="dropdown-menu">
          <a href="{{ route('admin.logout')}}" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
        </div>
      </li>
    </ul>
  </div>
</div>