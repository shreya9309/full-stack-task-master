<?php

namespace App\Domains\Http\Jobs\Respond\Show;

use Lucid\Foundation\Job;
use Illuminate\Routing\ResponseFactory;

class RespondWithJsonJob extends Job
{
    protected $status;
    protected $headers;
    protected $options;
    protected $type;
    protected $message;
    protected $content;

    public function __construct($status = 200,$headers = [], $options = 0, $type = 'data_found',$message = 'Data Available', $content = [])
    {
        $this->status = $status;
        $this->headers = $headers;
        $this->options = $options;
        $this->type = $type;
        $this->message = $message;
        $this->content = [
              'status' => 'success',
              'response' => [
                'code' => '200',
                'type' => $this->type,
                'message' => $this->message
              ],

        ];
        if($content){
           $this->content['data'] = $content;
        }
    }

    public function handle(ResponseFactory $factory)
    {
        return $factory->json($this->content, $this->status, $this->headers, $this->options);
    }
}
