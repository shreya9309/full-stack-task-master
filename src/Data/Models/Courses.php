<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class Courses extends Model
{
    protected $table = 'course_tbl';
  	protected $guarded = ['id'];
}
